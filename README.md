# simple fan control

Status: I still use it, but unmaintained.

This application controls Alya Networks based IOT ceiling fans. It currently works with Hunter fans.

Try the [web version](https://simplefancontrol.burkesoftware.com) or find it [Google Play](https://play.google.com/store/apps/details?id=com.burkesoftware.simplefancontrol). Or get the apk [here](https://gitlab.com/burke-software/simple-fan-control/-/blob/master/Simple_Fan_Control_base.apk).

This project is not affiliated with Alya Networks nor Hunter Fan Company in any way.

# Features

- Control your Internet connected fan
- Supports web, Android, and Wear OS
- Replaces (sometimes buggy) company provided apps for all features except device wifi registration.
- Respects your privacy as much as possible. Send less data to external vendors.

## Supported fans

- Hunter Fans - Advocate
- Any Hunter Fan should work. If it doesn't - open an issue as it should be a quick fix.

Please let us know if your device works by opening a issue or emailing info at burkesoftware dot com. You'll get a free coupon for the Google Play store version of the app for any contribution.

## Could be supported fans

Any fan that uses a Alya Networks IOT device can work with this. Open and issue if you'd like to help configure support.

We could probably control other Alya Networks IOT devices. We'd have to change the project name. Again open an issue if you want to help!

# How does it work

Alya Networks is a IOT device company. Any developer can work with their devices through their API. We simply implement the API controls and map settings to a specific ceiling fan model.

The web version is made possible by CORS anywhere. Burke Software does not store any logs of data sent to our CORS anywhere server. However for improved privacy we suggest using the app or self hosting.

# Improvements to consider

- Connect new IOT device to wifi
- Communicate directly to device instead of through Alya Networks
- Support more IOT devices than Alya Networks based boards
- Contact me if you want to port it to iOS. It should be very easy as we use NativeScript which already supports it. I just don't have the time, test devices, and money to do it myself.

# Commercial Support and Licensing

This project is under AGPLv3. If you need a license that is compatible with proprietary changes please contact info@burkesoftware.com

We are a consulting company and this is side project. If you are in the IOT industry and would like to consider our consulting services please contact us at info@burkesoftware.com

# Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`.

Run `tns run android` to run the app.

# Production server

- Change the [CORS Anywhere](https://github.com/Rob--W/cors-anywhere) server by editing `src/environments/environment.prod.ts`.
- Build the static html site `yarn build --prod`
- Host it on any web platform. We use Gitlab Pages.

## Building the Android App

See NativeScript [docs](https://docs.nativescript.org/tooling/publishing/publishing-android-apps).
