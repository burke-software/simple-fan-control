import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RouterExtensions } from "nativescript-angular/router";
import { UsersService } from "../alya/users.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [
      Validators.required,
      Validators.minLength(6)
    ])
  });
  error = "";

  constructor(private api: UsersService, private router: RouterExtensions) {}

  ngOnInit() {
    this.error = "";
  }

  onSubmit() {
    if (this.form.valid) {
      this.api
        .signIn(this.form.value.email, this.form.value.password)
        .toPromise()
        .then(() => this.router.navigate(["/home"], { clearHistory: true }))
        .catch((err: HttpErrorResponse) => {
          if (err.status === 401 && err.error && err.error.error) {
            this.error = err.error.error;
          } else {
            this.error =
              "Unable to log in. Did you enter your username and password correctly?";
          }
        });
    }
  }
}
