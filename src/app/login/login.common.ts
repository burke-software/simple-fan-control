import { Routes } from "@angular/router";
import { LoginComponent } from "./login.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SetPasswordComponent } from "./forgot-password/set-password/set-password.component";
import { ResendConfirmationComponent } from "./resend-confirmation/resend-confirmation.component";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent
  },
  {
    path: "forgot-password/set-password",
    component: SetPasswordComponent
  },
  {
    path: "resend-confirmation",
    component: ResendConfirmationComponent
  }
];
