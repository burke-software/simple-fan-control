import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";

import { LoginRoutingModule } from "./login-routing.module";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { LoginComponent } from "./login.component";
import { AlyaModule } from "../alya/alya.module";
import { SetPasswordComponent } from "./forgot-password/set-password/set-password.component";
import { SharedModule } from "../shared/shared.module";
import { ResendConfirmationComponent } from "./resend-confirmation/resend-confirmation.component";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    SetPasswordComponent,
    ResendConfirmationComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    SharedFormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    AlyaModule
  ]
})
export class LoginModule {}
