import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UsersService } from "~/app/alya/users.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-resend-confirmation",
  templateUrl: "./resend-confirmation.component.html",
  styleUrls: ["./resend-confirmation.component.scss"]
})
export class ResendConfirmationComponent {
  form = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email])
  });

  constructor(private usersService: UsersService, private router: Router) {}

  onSubmit() {
    this.usersService
      .resendUserConfirmation(this.form.value.email)
      .toPromise()
      .then(() => this.router.navigate(["/sign-up/confirmation"]));
  }
}
