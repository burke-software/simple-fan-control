import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { UsersService } from "~/app/alya/users.service";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent {
  error: string | null;
  form = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email])
  });

  constructor(private api: UsersService, private router: Router) {}

  onSubmit() {
    this.error = null;
    this.api
      .resendPassword(this.form.value.email)
      .toPromise()
      .then(() => this.router.navigate(["/login/forgot-password/set-password"]))
      .catch((err: HttpErrorResponse) => {
        if (
          err.status === 422 &&
          err.error &&
          err.error.errors &&
          err.error.errors.base
        ) {
          this.error = err.error.errors.base[0];
        } else {
          this.error = "Unable to Reset Password.";
        }
      });
  }
}
