import { Component } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { UsersService } from "~/app/alya/users.service";
import { SnackbarService } from "~/app/shared/snackbar.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-set-password",
  templateUrl: "./set-password.component.html",
  styleUrls: ["./set-password.component.scss"]
})
export class SetPasswordComponent {
  errorMessage: string;
  form = this.fb.group(
    {
      token: new FormControl("", [
        Validators.required,
        Validators.minLength(8)
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      passwordConfirm: new FormControl("", [Validators.required])
    },
    { validator: this.checkPasswords }
  );

  constructor(
    private api: UsersService,
    private fb: FormBuilder,
    private snackbar: SnackbarService,
    private router: Router
  ) {}

  onSubmit() {
    this.errorMessage = null;
    this.api
      .setPassword(this.form.value.token, this.form.value.password)
      .toPromise()
      .then(() => {
        this.snackbar.show("Successfully set new password");
        this.router.navigate(["/login"]);
      })
      .catch(err => (this.errorMessage = "Unable to set password"));
  }

  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.passwordConfirm.value;

    return pass === confirmPass ? null : { notSame: true };
  }
}
