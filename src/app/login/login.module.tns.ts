import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";
import { AlyaModule } from "../alya/alya.module";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SetPasswordComponent } from "./forgot-password/set-password/set-password.component";
import { SharedModule } from "../shared/shared.module";
import { ResendConfirmationComponent } from "./resend-confirmation/resend-confirmation.component";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    SetPasswordComponent,
    ResendConfirmationComponent
  ],
  imports: [
    LoginRoutingModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    ReactiveFormsModule,
    SharedModule,
    SharedFormsModule,
    AlyaModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule {}
