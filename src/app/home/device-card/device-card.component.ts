import { Component, Input, Output, EventEmitter } from "@angular/core";
import { IDeviceWithProperties, IProperty } from "~/app/alya/api.interfaces";

@Component({
  selector: "app-device-card",
  templateUrl: "./device-card.component.html",
  styleUrls: ["./device-card.component.scss"]
})
export class DeviceCardComponent {
  private _device: IDeviceWithProperties;
  @Output() toggleProperty = new EventEmitter<IProperty>();
  @Output() sliderChange = new EventEmitter<{
    prop: IProperty;
    value: string;
  }>();
  downdraftCtl: IProperty | null;

  @Input("device") set device(device: IDeviceWithProperties) {
    this.downdraftCtl = device.properties.find(
      prop => prop.name === "SET_FAN_DOWNDRAFT_CTL"
    );
    this._device = device;
  }

  get device(): IDeviceWithProperties {
    return this._device;
  }

  onSliderChange(value: number, prop: IProperty) {
    this.sliderChange.emit({ prop, value: value.toString() });
  }

  getDowndraftLabel() {
    if (this.downdraftCtl) {
      if (this.downdraftCtl.value === 0) {
        return "Change to Summer mode";
      } else {
        return "Change to Winter mode";
      }
    }
  }

  toggleDowndraft() {
    this.toggleProperty.emit(this.downdraftCtl);
  }
}
