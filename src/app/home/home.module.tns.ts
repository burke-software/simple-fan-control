import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { DeviceCardComponent } from "./device-card/device-card.component";
import { HomeContainer } from "./home.container";
import { SharedModule } from "../shared/shared.module";
import { OnboardingComponent } from "./onboarding/onboarding.component";
import { SliderModule } from "../slider/slider.module";

@NgModule({
  declarations: [
    HomeComponent,
    HomeContainer,
    DeviceCardComponent,
    OnboardingComponent
  ],
  imports: [
    NativeScriptCommonModule,
    HomeRoutingModule,
    SliderModule,
    SharedModule
  ],
  exports: [HomeComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule {}
