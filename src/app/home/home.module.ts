import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { HttpClientModule } from "@angular/common/http";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { HomeContainer } from "./home.container";
import { DeviceCardComponent } from "./device-card/device-card.component";
import { SharedModule } from "../shared/shared.module";
import { OnboardingComponent } from "./onboarding/onboarding.component";
import { SliderModule } from "../slider/slider.module";

@NgModule({
  declarations: [
    HomeComponent,
    HomeContainer,
    DeviceCardComponent,
    OnboardingComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatGridListModule,
    MatMenuModule,
    MatDividerModule,
    HttpClientModule,
    MatButtonModule,
    SharedModule,
    SliderModule
  ],
  exports: [HomeComponent]
})
export class HomeModule {}
