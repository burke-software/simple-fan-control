import { Component, OnInit, OnDestroy } from "@angular/core";
import { timer, Subscription, EMPTY } from "rxjs";
import { concatMap, catchError } from "rxjs/operators";
import { UsersService } from "../alya/users.service";
import { DevicesService } from "../alya/devices.service";
import { IDeviceWithProperties, IProperty } from "../alya/api.interfaces";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  template: `
    <app-home
      [devices]="devices"
      [hasLoaded]="hasLoaded"
      (sliderChange)="onSliderChange($event)"
      (toggleProperty)="toggleProperty($event)"
      (signOut)="signOut()"
    ></app-home>
  `
})
export class HomeContainer implements OnInit, OnDestroy {
  devices: IDeviceWithProperties[] = [];
  polling$: Subscription;
  hasLoaded = false;

  constructor(
    public devicesService: DevicesService,
    public userService: UsersService
  ) {
    this.devicesService.getDevicesWithProperties.subscribe(
      devices => (this.devices = devices)
    );
  }

  ngOnInit() {
    this.devicesService
      .retrieveAll()
      .toPromise()
      .then(() => (this.hasLoaded = true));
    const pollInterval = 10000;
    this.polling$ = timer(pollInterval, pollInterval)
      .pipe(
        concatMap(() =>
          this.devicesService
            .updateProperties()
            .pipe(catchError((err: HttpErrorResponse) => EMPTY))
        )
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.polling$.unsubscribe();
  }

  getTitle() {
    if (this.devices.length) {
      return this.devices[0].productName;
    }
    return "Fans";
  }

  toggleProperty(property: IProperty) {
    const oldValue = property.value;
    let newValue = "";
    if (oldValue === "0" || oldValue === 0) {
      newValue = "1";
    } else if (oldValue === "1" || oldValue === 1) {
      newValue = "0";
    }
    this.devicesService.createDatapoint(property.key, newValue).toPromise();
  }

  onSliderChange(event: { value: string; prop: IProperty }) {
    this.setProperty({
      property: event.prop,
      value: event.value
    });
  }

  setProperty(event: { property: IProperty; value: string }) {
    this.devicesService
      .createDatapoint(event.property.key, event.value)
      .toPromise();
  }

  signOut() {
    this.userService.signOut().toPromise();
  }
}
