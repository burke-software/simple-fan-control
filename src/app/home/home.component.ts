import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { IDeviceWithProperties, IProperty } from "../alya/api.interfaces";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  private _devices: IDeviceWithProperties[] = [];
  @Input() hasLoaded = false;
  @Output() toggleProperty = new EventEmitter<IProperty>();
  @Output() sliderChange = new EventEmitter<{
    value: string;
    prop: IProperty;
  }>();
  @Output() setProperty = new EventEmitter<{
    property: IProperty;
    value: string;
  }>();
  @Output() signOut = new EventEmitter();
  /** NS pager */
  currentPagerIndex = 0;
  title: string;
  downdraftCtl: IProperty | null;
  downdraftLabel: string | null;

  @Input("devices") set devices(devices: IDeviceWithProperties[]) {
    this._devices = devices;
    this.getDowndraftProperty();
    this.getDowndraftLabel();
    this.title = this.getTitle();
  }

  get devices(): IDeviceWithProperties[] {
    return this._devices;
  }

  ngOnInit() {
    this.title = this.getTitle();
  }

  getTitle() {
    if (this.devices.length) {
      if (this.currentPagerIndex) {
        return this.devices[this.currentPagerIndex].productName;
      }
      return this.devices[0].productName;
    }
  }

  getDowndraftProperty() {
    this.downdraftCtl = null;
    const device = this.devices[this.currentPagerIndex];
    if (device) {
      this.downdraftCtl = device.properties.find(
        (prop) => prop.name === "SET_FAN_DOWNDRAFT_CTL"
      );
    }
  }

  onPagerIndexChanged(args: any) {
    this.currentPagerIndex = args.index;
    this.title = this.getTitle();
    this.getDowndraftProperty();
    this.getDowndraftLabel();
  }

  getDowndraftLabel() {
    this.downdraftLabel = null;
    if (this.downdraftCtl) {
      if (this.downdraftCtl.value === 0) {
        this.downdraftLabel = "Change to Summer mode";
      } else {
        this.downdraftLabel = "Change to Winter mode";
      }
    }
  }

  toggleDowndraft() {
    this.toggleProperty.emit(this.downdraftCtl);
  }
}
