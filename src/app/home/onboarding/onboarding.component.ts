import { Component } from "@angular/core";
import { sampleFan } from "../../demo/home-demo/demo-data";

@Component({
  selector: "app-onboarding",
  templateUrl: "./onboarding.component.html",
  styleUrls: ["./onboarding.component.scss"]
})
export class OnboardingComponent {
  simpleConnectGooglePlay =
    "https://play.google.com/store/apps/details?id=com.hunter.agilelink";
  simpleConnectAppStore =
    "https://apps.apple.com/us/app/simpleconnect-wi-fi/id1055929747";
  simpleFanControlGooglePlay =
    "https://play.google.com/store/apps/details?id=com.burkesoftware.simplefancontrol"
  sampleFan = sampleFan;

  bloop() {}
}
