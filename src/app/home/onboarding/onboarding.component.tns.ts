import { Component } from "@angular/core";
import * as utils from "tns-core-modules/utils/utils";

@Component({
  selector: "app-onboarding",
  templateUrl: "./onboarding.component.html",
  styleUrls: ["./onboarding.component.scss"]
})
export class OnboardingComponent {
  simpleConnectGooglePlay =
    "https://play.google.com/store/apps/details?id=com.hunter.agilelink";
  simpleFanControlWebApp =
    "https://burke-software.gitlab.io/simple-fan-control/";

  openSimpleconnectGooglePlay() {
    utils.openUrl(this.simpleConnectGooglePlay);
  }

  openWebApp() {
    utils.openUrl(this.simpleFanControlWebApp);
  }
}
