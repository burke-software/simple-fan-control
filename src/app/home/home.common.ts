import { Routes } from "@angular/router";
import { HomeContainer } from "./home.container";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: HomeContainer
  }
];
