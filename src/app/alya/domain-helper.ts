import { environment } from "~/environments/environment";

/** For local dev, use a local proxy. For production, use a hosted CORS Anywhere */
export function getDomain(domain: string, basePath: string) {
  if (environment.corsAnywhere) {
    return environment.corsAnywhere + domain + basePath;
  }
  // Local proxy
  return basePath;
}
