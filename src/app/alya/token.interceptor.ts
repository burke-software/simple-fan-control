import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, exhaustMap, switchMap } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { UsersService } from "./users.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  refreshToken: string;
  expiresAt: Date;
  isRefreshing = false;

  constructor(public auth: AuthService, public users: UsersService) {
    this.auth.data.subscribe(data => {
      this.refreshToken = data.refreshToken;
      this.expiresAt = data.expiresAt;
    });
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.isRefreshing && this.expiresAt && new Date() > this.expiresAt) {
      this.isRefreshing = true;
      return this.users.refreshToken(this.refreshToken).pipe(
        switchMap(resp => {
          this.isRefreshing = false;
          req = req.clone({
            setHeaders: { Authorization: resp.access_token }
          });
          return this.attemptRequest(req, next);
        }),
        catchError(err => {
          // check if refresh token is invalid
          if (
            this.refreshToken &&
            err &&
            err.status === 401 &&
            err.error &&
            err.error.error === "Your refresh token is invalid"
          ) {
            this.isRefreshing = false;
            this.users.clearAuthAndLogin();
          } else {
            return throwError(err);
          }
        })
      );
    }
    return this.attemptRequest(req, next);
  }

  private attemptRequest(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (
          this.refreshToken &&
          !this.isRefreshing &&
          error &&
          error.status === 401
        ) {
          this.isRefreshing = true;
          return this.users.refreshToken(this.refreshToken).pipe(
            exhaustMap(resp => {
              req = req.clone({
                setHeaders: { Authorization: resp.access_token }
              });
              return next.handle(req);
            }),
            catchError(err => {
              // Logout
              this.auth.clearAuth();
              this.isRefreshing = false;
              return throwError(error);
            })
          );
        } else {
          this.isRefreshing = false;
          return throwError(error);
        }
      })
    );
  }
}
