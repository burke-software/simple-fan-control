import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  BehaviorSubject,
  forkJoin,
  combineLatest,
  Observable,
  EMPTY
} from "rxjs";
import { map, tap, exhaustMap, withLatestFrom, delay } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { baseHeaders } from "./api.constants";
import {
  IDevice,
  IDeviceResp,
  IPropertiesResp,
  IProperty,
  IDeviceWithProperties,
  IDatapointsResp
} from "./api.interfaces";
import { getDomain } from "./domain-helper";

const storageDevicesKey = "devices";
const storagePropertiesKey = "properties";

const basePath = "/apiv1";
const domain = "https://ads-field.aylanetworks.com";
const baseUrl = getDomain(domain, basePath);

const lightProperty = "SET_LIGHT0_ON_CTL";
const lightIlluminationProperty = "SET_LIGHT0_ILLUMINATION_CTL";
const fanOnProperty = "SET_FAN_ON_CTL";
const fanSpeedProperty = "SET_FAN_SPEED_CTL";
const fanDowndraftProperty = "SET_FAN_DOWNDRAFT_CTL";
/* Sorted properties we want for each device */
const desiredProperties = [
  lightProperty,
  lightIlluminationProperty,
  fanOnProperty,
  fanSpeedProperty,
  fanDowndraftProperty
];

@Injectable({
  providedIn: "root"
})
export class DevicesService {
  accessToken: string | null;
  private devices = new BehaviorSubject<IDevice[]>([]);
  private _devices: IDevice[];
  getDevices = this.devices.asObservable();
  private properties = new BehaviorSubject<IProperty[]>([]);
  private _properties: IProperty[];
  getProperties = this.properties.asObservable();
  getDevicesWithProperties: Observable<IDeviceWithProperties[]> = combineLatest(
    this.getDevices,
    this.getProperties
  ).pipe(
    map(([devices, properties]) => {
      return devices.map(device => {
        const deviceProperties = properties
          .filter(prop => prop.deviceKey === device.key)
          .sort((a, b) => {
            // Sort by explicit name
            if (
              desiredProperties.indexOf(a.name) >
              desiredProperties.indexOf(b.name)
            ) {
              return 1;
            }
            return -1;
          });
        return {
          ...device,
          properties: deviceProperties
        };
      });
    })
  );

  constructor(private http: HttpClient, private auth: AuthService) {
    this.auth.getAuthToken.subscribe(token => (this.accessToken = token));
    this.devices.subscribe(devices => (this._devices = devices));
    this.properties.subscribe(properties => (this._properties = properties));
    const devicesData = localStorage.getItem(storageDevicesKey);
    if (devicesData) {
      const devices: IDevice[] = JSON.parse(devicesData);
      if (devices.length > 0 && devices[0].productName) {
        this.setDevices(devices);
      }
    }
    const propertiesData = localStorage.getItem(storagePropertiesKey);
    if (propertiesData) {
      const properties: IProperty[] = JSON.parse(propertiesData);
      this.setProperties(properties);
    }
  }

  retrieveAll() {
    return this.retrieveDevices().pipe(
      exhaustMap(devices => {
        const deviceRequests = devices.map(device =>
          this.retrieveProperies(device.dsn)
        );
        return forkJoin(deviceRequests).pipe(map(() => EMPTY));
      })
    );
  }

  retrieveDevices() {
    const headers = {
      ...baseHeaders,
      Authorization: this.accessToken
    };
    const url = baseUrl + "/devices";
    return this.http.get<IDeviceResp[]>(url, { headers }).pipe(
      map(devices =>
        devices.map(deviceResp => {
          const device = deviceResp.device;
          const result: IDevice = {
            key: device.key,
            productName: device.product_name,
            dsn: device.dsn
          };
          return result;
        })
      ),
      tap(data => this.setDevices(data))
    );
  }

  /** Update all device properties */
  updateProperties() {
    // Update devices list often when there are none.
    if (this._devices.length === 0) {
      this.retrieveDevices().toPromise();
    }
    const deviceRequests = this._devices.map(device =>
      this.retrieveProperies(device.dsn)
    );
    return forkJoin(deviceRequests);
  }

  retrieveProperies(dsn: string) {
    const headers = {
      ...baseHeaders,
      Authorization: this.accessToken
    };
    const url = baseUrl + "/dsns/" + dsn + "/properties";
    const params = {
      "names[]": desiredProperties
    };
    return this.http.get<IPropertiesResp[]>(url, { headers, params }).pipe(
      map(properties =>
        properties.map(propertyResp => {
          const prop = propertyResp.property;
          const result: IProperty = {
            deviceKey: prop.device_key,
            baseType: prop.base_type,
            key: prop.key,
            name: prop.name,
            value: prop.value
          };
          return result;
        })
      ),
      tap(data => this.setProperties(data))
    );
  }

  createDatapoint(propertyId: number, value: string) {
    const headers = {
      ...baseHeaders,
      Authorization: this.accessToken
    };
    const url = baseUrl + "/properties/" + propertyId + "/datapoints";
    const data = {
      datapoint: {
        value
      }
    };
    return this.http.post<IDatapointsResp>(url, data, { headers }).pipe(
      withLatestFrom(this.properties),
      tap(([datapoint, properties]) => {
        const propertyToUpdateI = properties.findIndex(
          property => property.key === propertyId
        );
        if (propertyToUpdateI !== -1) {
          const newProperties = [...properties];
          newProperties[propertyToUpdateI] = {
            ...properties[propertyToUpdateI],
            value: datapoint.datapoint.value
          };
          this.setProperties(newProperties);
        }
      }),
      delay(2000), // Wait 2 seconds and update other properties. We don't know how long it will take Alya to update.
      tap(() => this.updateProperties().toPromise()),
      delay(3000), // Typically by 5 seconds we get the update
      tap(() => this.updateProperties().toPromise())
    );
  }

  setDevices(data: IDevice[]) {
    this.devices.next(data);
    localStorage.setItem(storageDevicesKey, JSON.stringify(data));
  }

  setProperties(data: IProperty[]) {
    // Check if changed
    if (!this.deepEqual(data, this._properties)) {
      this.properties.next(data);
      localStorage.setItem(storagePropertiesKey, JSON.stringify(data));
    }
  }

  clearDevices() {
    this.devices.next([]);
  }

  /** Simplistic deep equal implementation */
  private deepEqual(x, y) {
    const ok = Object.keys,
      tx = typeof x,
      ty = typeof y;
    return x && y && tx === "object" && tx === ty
      ? ok(x).length === ok(y).length &&
          ok(x).every(key => this.deepEqual(x[key], y[key]))
      : x === y;
  }
}
