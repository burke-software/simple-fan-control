interface IApplication {
  app_id: string;
  app_secret: string;
}

export interface ISignInPayload {
  user: {
    email: string;
    password: string;
    application: IApplication;
  };
}

export interface ISignInResp {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  role: string;
  role_tags: string[];
}

export interface IUserSignUp {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  country: string;
  city: string;
  street: string;
  zip: string;
  phone_country_code?: string;
  phone?: string;
}

interface IUserSignUpForPayload extends IUserSignUp {
  application: IApplication;
}

export interface ISignUpPayload {
  user: IUserSignUpForPayload;
}

export interface IResendPasswordPayload {
  user: {
    email: string;
    application: IApplication;
  };
}

export interface IResendUserConfirmationPayload
  extends IResendPasswordPayload {}

export interface ISetPasswordPayload {
  user: {
    reset_password_token: string;
    password: string;
    password_confirmation: string;
  };
}

export interface IChangePasswordPayload {
  user: {
    current_password: string;
    password: string;
  };
}

export interface IAuth {
  accessToken: string;
  refreshToken: string;
  expiresAt: Date;
}

type ConnectionStatus = "Online" | "Offline" | "Initializing";
type DeviceType = "Wifi" | "Gateway" | "Node";

export interface IDevice {
  key: number;
  productName: string;
  dsn: string;
}

export interface IDeviceResp {
  device: {
    product_name: string;
    model: string;
    dsn: string;
    oem_model: string;
    sw_version: string;
    template_id: number;
    mac: string;
    unique_hardware_id: any | null;
    lan_ip: string;
    connected_at: string;
    key: number;
    lan_enabled: boolean;
    has_properties: boolean;
    product_class: any | null;
    connection_status: ConnectionStatus;
    lat: number;
    lng: number;
    locality: number;
    device_type: DeviceType;
  };
}

export interface IProperty {
  name: string;
  key: number;
  deviceKey: number;
  value: string | number;
  baseType: "integer" | "string" | "boolean" | "decimal" | "file";
}

export interface IDeviceWithProperties extends IDevice {
  properties: IProperty[];
}

export interface IPropertiesResp {
  property: {
    type: "Property";
    name: string;
    base_type: "integer" | "string" | "boolean" | "decimal" | "file";
    read_only: boolean;
    direction: "input" | "output";
    scope: "user" | "oem";
    data_updated_at: string;
    key: number;
    device_key: number;
    product_name: string;
    track_only_changes: boolean;
    display_name: string;
    host_sw_version: boolean;
    time_series: boolean;
    derived: boolean;
    app_type: string | null;
    recipe: string | null;
    value: string;
    denied_roles: any[];
    ack_enabled: number | null;
    retention_days: number | null;
  };
}

export interface IDatapointsResp {
  datapoint: {
    created_at: string;
    updated_at: string;
    echo: boolean;
    value: string;
    metadata: any;
  };
}
