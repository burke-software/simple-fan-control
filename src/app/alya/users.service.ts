import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { tap } from "rxjs/operators";
import {
  ISignInPayload,
  ISignInResp,
  IResendPasswordPayload,
  ISetPasswordPayload,
  ISignUpPayload,
  IUserSignUp,
  IResendUserConfirmationPayload,
  IChangePasswordPayload
} from "./api.interfaces";
import { appId, appSecret, baseHeaders } from "./api.constants";
import { AuthService } from "./auth.service";
import { getDomain } from "./domain-helper";
import { Router } from "@angular/router";

const domain = "https://user.aylanetworks.com";
const basePath = "/users";
const baseUrl = getDomain(domain, basePath);
const headers = {
  Accept: "application/json"
};

@Injectable({
  providedIn: "root"
})
export class UsersService {
  accessToken: string | null;
  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private router: Router
  ) {
    this.auth.getAuthToken.subscribe(token => (this.accessToken = token));
  }

  signIn(email: string, password: string) {
    const url = baseUrl + "/sign_in";
    const data: ISignInPayload = {
      user: {
        email,
        password,
        application: {
          app_id: appId,
          app_secret: appSecret
        }
      }
    };
    return this.http
      .post<ISignInResp>(url, data, { headers })
      .pipe(tap(resp => this.setAuth(resp)));
  }

  signUp(user: IUserSignUp) {
    const url = baseUrl;
    const data: ISignUpPayload = {
      user: {
        ...user,
        application: {
          app_id: appId,
          app_secret: appSecret
        }
      }
    };
    return this.http.post(url, data, { headers });
  }

  signOut() {
    const url = baseUrl + "/sign_out";
    const data = {
      user: {
        access_token: this.accessToken
      }
    };
    return this.http
      .post(url, data, { headers })
      .pipe(tap(() => this.clearAuthAndLogin()));
  }

  delete() {
    const url = baseUrl;
    const deleteHeaders = {
      ...baseHeaders,
      Authorization: "auth_token " + this.accessToken
    };
    return this.http
      .delete(url, { headers: deleteHeaders })
      .pipe(tap(() => this.clearAuthAndLogin()));
  }

  clearAuthAndLogin() {
    this.auth.clearAuth();
    this.router.navigate(["/login"]);
  }

  changePassword(oldPassword: string, newPassword: string) {
    const url = baseUrl;
    const authHeaders = {
      ...baseHeaders,
      Authorization: "auth_token " + this.accessToken
    };
    const data: IChangePasswordPayload = {
      user: {
        password: newPassword,
        current_password: oldPassword
      }
    };
    return this.http.put(url, data, { headers: authHeaders });
  }

  confirmation(confirmationToken: string) {
    const url = baseUrl + "/confirmation";
    const params = {
      confirmation_token: confirmationToken
    };
    return this.http.put(url, {}, { params, headers });
  }

  resendUserConfirmation(email: string) {
    const data: IResendUserConfirmationPayload = {
      user: {
        email,
        application: {
          app_id: appId,
          app_secret: appSecret
        }
      }
    };
    const url = baseUrl + "/confirmation";
    return this.http.post<{}>(url, data, { headers });
  }

  refreshToken(refreshToken: string) {
    const url = baseUrl + "/refresh_token";
    const data = {
      user: {
        refresh_token: refreshToken
      }
    };
    return this.http
      .post<ISignInResp>(url, data, { headers })
      .pipe(tap(resp => this.setAuth(resp)));
  }

  setAuth(resp: ISignInResp) {
    const now = new Date();
    now.setSeconds(now.getSeconds() + resp.expires_in);
    this.auth.setAuth({
      accessToken: resp.access_token,
      expiresAt: now,
      refreshToken: resp.refresh_token
    });
  }

  /** Resend Password Reset Instructions */
  resendPassword(email: string) {
    const data: IResendPasswordPayload = {
      user: {
        email,
        application: {
          app_id: appId,
          app_secret: appSecret
        }
      }
    };
    const url = baseUrl + "/password";
    return this.http.post<{}>(url, data, { headers });
  }

  setPassword(token: string, password: string) {
    const data: ISetPasswordPayload = {
      user: {
        reset_password_token: token,
        password,
        password_confirmation: password
      }
    };
    const url = baseUrl + "/password";
    return this.http.put<{}>(url, data, { headers });
  }
}
