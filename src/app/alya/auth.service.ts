import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
import { IAuth } from "./api.interfaces";

const initialState: IAuth = {
  accessToken: null,
  expiresAt: null,
  refreshToken: null
};

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private authData = new BehaviorSubject<IAuth>(initialState);
  data = this.authData.asObservable();
  isLoggedIn = this.data.pipe(map(data => Boolean(data.accessToken)));
  getAuthToken = this.authData.pipe(map(auth => auth.accessToken));

  constructor() {
    const authData = localStorage.getItem("auth");
    if (authData) {
      const auth = JSON.parse(authData);
      if (auth.accessToken) {
        this.setAuth({
          accessToken: auth.accessToken,
          expiresAt: new Date(Date.parse(auth.expiresAt)),
          refreshToken: auth.refreshToken
        });
      }
    }
  }

  setAuth(data: IAuth) {
    this.authData.next(data);
    localStorage.setItem("auth", JSON.stringify(data));
  }

  clearAuth() {
    this.authData.next(initialState);
    localStorage.clear();
  }
}
