import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-text-input",
  templateUrl: "./text-input.component.html",
  styleUrls: ["./text-input.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextInputComponent {
  @Input() control: FormControl;
  @Input() placeholder: string;
  @Input() type: string;
  @Input() returnKeyType = "next";
  @Input() errorMessage: string;
  @Input() hint: string;
  @Input() autofocus = false;
  @Output() returnPress = new EventEmitter();

  getKeyboardType() {
    if (this.type === "email") {
      return "email";
    }
  }

  getSecure() {
    if (this.type === "password") {
      return true;
    }
  }
}
