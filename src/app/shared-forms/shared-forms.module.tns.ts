import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TextInputComponent } from "./text-input/text-input.component";

@NgModule({
  declarations: [TextInputComponent],
  imports: [
    NativeScriptCommonModule,
    ReactiveFormsModule,
    NativeScriptFormsModule
  ],
  exports: [TextInputComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedFormsModule {}
