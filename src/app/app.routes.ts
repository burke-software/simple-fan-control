import { Routes } from "@angular/router";

import { IsLoggedInGuard } from "./guards/is-logged-in.guard";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    // redirectTo: "/demo",
    pathMatch: "full",
  },
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((mod) => mod.HomeModule),
    canActivate: [IsLoggedInGuard],
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then((mod) => mod.LoginModule),
  },
  {
    path: "sign-up",
    loadChildren: () =>
      import("./sign-up/sign-up.module").then((mod) => mod.SignUpModule),
  },
  {
    path: "account",
    loadChildren: () =>
      import("./account/account.module").then((mod) => mod.AccountModule),
    canActivate: [IsLoggedInGuard],
  },
  {
    path: "demo",
    loadChildren: () =>
      import("./demo/demo.module").then((mod) => mod.DemoModule),
  },
  {
    path: "debug",
    loadChildren: () =>
      import("./debug/debug.module").then((mod) => mod.DebugModule),
  },
  {
    path: "privacy",
    loadChildren: () =>
      import("./privacy/privacy.module").then((mod) => mod.PrivacyModule),
  },
  { path: "**", redirectTo: "/" },
];
