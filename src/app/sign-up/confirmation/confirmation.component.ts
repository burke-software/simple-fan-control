import { Component } from "@angular/core";
import { Validators, FormControl, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";

import { UsersService } from "~/app/alya/users.service";
import { SnackbarService } from "~/app/shared/snackbar.service";

@Component({
  selector: "app-confirmation",
  templateUrl: "./confirmation.component.html",
  styleUrls: ["./confirmation.component.scss"]
})
export class ConfirmationComponent {
  form = this.fb.group({
    confirmationToken: new FormControl("", [
      Validators.required,
      Validators.minLength(6)
    ])
  });

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private router: Router,
    private snackbar: SnackbarService
  ) {}

  onSubmit() {
    if (this.form.valid) {
      this.usersService
        .confirmation(this.form.value.confirmationToken)
        .toPromise()
        .then(() => {
          this.snackbar.show("Account confirmed");
          this.router.navigate(["/login"]);
        })
        .catch(() => this.snackbar.show("Unable to confirm account"));
    }
  }
}
