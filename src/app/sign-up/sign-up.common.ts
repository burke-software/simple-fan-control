import { Routes } from "@angular/router";
import { SignUpComponent } from "./sign-up.component";
import { ConfirmationComponent } from "./confirmation/confirmation.component";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: SignUpComponent
  },
  {
    path: "confirmation",
    component: ConfirmationComponent
  }
];
