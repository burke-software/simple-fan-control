import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { SignUpComponent } from "./sign-up.component";
import { SignUpRoutingModule } from "./sign-up-routing.module";
import { SharedModule } from "../shared/shared.module";
import { ConfirmationComponent } from "./confirmation/confirmation.component";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";

@NgModule({
  declarations: [SignUpComponent, ConfirmationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatListModule,
    MatSnackBarModule,
    SignUpRoutingModule,
    SharedModule,
    SharedFormsModule
  ]
})
export class SignUpModule {}
