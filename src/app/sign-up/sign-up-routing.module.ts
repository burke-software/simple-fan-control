import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { routes } from "./sign-up.common";

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SignUpRoutingModule {}
