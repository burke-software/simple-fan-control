import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup
} from "@angular/forms";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

import { UsersService } from "../alya/users.service";

@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"]
})
export class SignUpComponent implements OnInit {
  form = this.fb.group(
    {
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(
          "(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{6,}"
        )
      ]),
      passwordConfirm: new FormControl("", [Validators.required]),
      firstname: new FormControl("", Validators.required),
      lastname: new FormControl("", Validators.required),
      country: new FormControl(""),
      city: new FormControl(""),
      street: new FormControl(""),
      zip: new FormControl("")
    },
    { validator: this.checkPasswords }
  );
  error = "";

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private router: Router
  ) {}

  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.passwordConfirm.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  ngOnInit() {
    this.error = "";
  }

  onSubmit() {
    this.error = "";
    if (this.form.valid) {
      this.usersService
        .signUp({
          email: this.form.value.email,
          password: this.form.value.password,
          firstname: this.form.value.firstname,
          lastname: this.form.value.lastname,
          country: this.form.value.country,
          city: this.form.value.city,
          street: this.form.value.street,
          zip: this.form.value.zip
        })
        .toPromise()
        .then(() => this.router.navigate(["/sign-up/confirmation"]))
        .catch((err: HttpErrorResponse) => {
          if (
            err.status === 422 &&
            err.error &&
            err.error.errors &&
            err.error.errors.email &&
            err.error.errors.email[0] === "has already been taken"
          ) {
            this.error =
              "Email is already taken. You may need to resend email confirmation or reset your password.";
          } else {
            this.error = "Unable to create account";
          }
        });
    }
  }
}
