import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { SignUpComponent } from "./sign-up.component";
import { SignUpRoutingModule } from "./sign-up-routing.module";
import { ConfirmationComponent } from "./confirmation/confirmation.component";
import { SharedModule } from "../shared/shared.module";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";
@NgModule({
  declarations: [SignUpComponent, ConfirmationComponent],
  imports: [
    NativeScriptCommonModule,
    SignUpRoutingModule,
    NativeScriptFormsModule,
    ReactiveFormsModule,
    SharedModule,
    SharedFormsModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SignUpModule {}
