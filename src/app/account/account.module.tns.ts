import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { AccountRoutingModule } from "./account-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { AccountComponent } from "./account.component";
import { SharedModule } from "../shared/shared.module";
import { AlyaModule } from "../alya/alya.module";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";

@NgModule({
  declarations: [AccountComponent],
  imports: [
    AccountRoutingModule,
    NativeScriptCommonModule,
    SharedModule,
    SharedFormsModule,
    AlyaModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AccountModule {}
