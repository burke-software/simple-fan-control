import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";

import { AccountRoutingModule } from "./account-routing.module";
import { AccountComponent } from "./account.component";
import { SharedModule } from "../shared/shared.module";
import { AlyaModule } from "../alya/alya.module";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedFormsModule } from "../shared-forms/shared-forms.module";

@NgModule({
  declarations: [AccountComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountRoutingModule,
    MatButtonModule,
    MatCardModule,
    SharedModule,
    SharedFormsModule,
    AlyaModule
  ]
})
export class AccountModule {}
