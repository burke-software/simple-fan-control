import { Component, OnInit } from "@angular/core";
import { UsersService } from "../alya/users.service";
import { SnackbarService } from "~/app/shared/snackbar.service";
import {
  FormControl,
  Validators,
  FormBuilder,
  FormGroup
} from "@angular/forms";

@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss"]
})
export class AccountComponent implements OnInit {
  form = this.fb.group(
    {
      oldPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(
          "(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{6,}"
        )
      ]),
      passwordConfirm: new FormControl("", [Validators.required])
    },
    { validator: this.checkPasswords }
  );

  constructor(
    public userService: UsersService,
    private fb: FormBuilder,
    private snackbar: SnackbarService
  ) {}

  ngOnInit() {}

  signOut() {
    this.userService.signOut().toPromise();
  }

  warnAndDelete() {
    const result = confirm(
      "Are you sure you want to permanantly delete your account for all Alya Networks and Simpleconnect Wifi apps?"
    );
    if (result) {
      this.userService.delete().toPromise();
    }
  }

  changePassword() {
    if (this.form.valid) {
      this.userService
        .changePassword(this.form.value.oldPassword, this.form.value.password)
        .toPromise()
        .then(() => {
          this.snackbar.show("Successfully set new password");
          this.userService.clearAuthAndLogin();
        })
        .catch(() => this.snackbar.show("Unable change password"));
    }
  }

  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.passwordConfirm.value;

    return pass === confirmPass ? null : { notSame: true };
  }
}
