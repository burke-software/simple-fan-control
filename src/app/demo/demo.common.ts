import { Routes } from "@angular/router";
import { HomeDemoComponent } from "./home-demo/home-demo.component";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: HomeDemoComponent
  }
];
