import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DemoRoutingModule } from "./demo-routing.module";
import { HomeDemoComponent } from "./home-demo/home-demo.component";
import { HomeModule } from "../home/home.module";

@NgModule({
  declarations: [HomeDemoComponent],
  imports: [CommonModule, DemoRoutingModule, HomeModule]
})
export class DemoModule {}
