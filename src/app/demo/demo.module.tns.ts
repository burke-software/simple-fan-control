import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DemoRoutingModule } from "./demo-routing.module";
import { HomeDemoComponent } from "./home-demo/home-demo.component";
import { HomeModule } from "../home/home.module";

@NgModule({
  declarations: [HomeDemoComponent],
  imports: [DemoRoutingModule, NativeScriptCommonModule, HomeModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DemoModule {}
