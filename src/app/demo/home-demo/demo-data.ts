export const device1 = {
  key: 16374560,
  productName: "Ayla TQS",
  dsn: "AC000Q312223363",
  properties: [
    {
      deviceKey: 16374560,
      baseType: "boolean",
      key: 149614000,
      name: "SET_LIGHT0_ON_CTL",
      value: 0
    },
    {
      deviceKey: 16374560,
      baseType: "integer",
      key: 149613001,
      name: "SET_LIGHT0_ILLUMINATION_CTL",
      value: 0
    },
    {
      deviceKey: 16374560,
      baseType: "boolean",
      key: 149614002,
      name: "SET_FAN_ON_CTL",
      value: 1
    },
    {
      deviceKey: 16374560,
      baseType: "integer",
      key: 149613003,
      name: "SET_FAN_SPEED_CTL",
      value: 25
    }
  ]
};

export const device2 = {
  key: 16374561,
  productName: "Ayla QAT",
  dsn: "AC000Q312223364",
  properties: [
    {
      deviceKey: 16374561,
      baseType: "boolean",
      key: 149610000,
      name: "SET_LIGHT0_ON_CTL",
      value: 1
    },
    {
      deviceKey: 16374561,
      baseType: "integer",
      key: 149610001,
      name: "SET_LIGHT0_ILLUMINATION_CTL",
      value: 100
    },
    {
      deviceKey: 16374561,
      baseType: "boolean",
      key: 149610002,
      name: "SET_FAN_ON_CTL",
      value: 1
    },
    {
      deviceKey: 16374561,
      baseType: "integer",
      key: 149610003,
      name: "SET_FAN_SPEED_CTL",
      value: 100
    }
  ]
};

export const sampleFan = {
  key: 16374563,
  productName: "Sample Fan",
  dsn: "AC000Q312223364",
  properties: [
    {
      deviceKey: 16374561,
      baseType: "boolean",
      key: 149610000,
      name: "SET_LIGHT0_ON_CTL",
      value: 0
    },
    {
      deviceKey: 16374561,
      baseType: "integer",
      key: 149610001,
      name: "SET_LIGHT0_ILLUMINATION_CTL",
      value: 0
    },
    {
      deviceKey: 16374561,
      baseType: "boolean",
      key: 149610002,
      name: "SET_FAN_ON_CTL",
      value: 0
    },
    {
      deviceKey: 16374561,
      baseType: "integer",
      key: 149610003,
      name: "SET_FAN_SPEED_CTL",
      value: 0
    }
  ]
};

export type DeviceState = "all-on" | "all-off" | "random";
const makeKey = (number: number) => ("00000000" + number).slice(-8);
const makeValueBoolean = (deviceState: DeviceState) => {
  if (deviceState === "all-off") return 0;
  if (deviceState === "all-on") return 1;
  if (deviceState === "random") return Math.round(Math.random());
};
const makeValueInteger = (deviceState: DeviceState, booleanValue: number) => {
  if (booleanValue === 0) return 0;
  if (booleanValue === 1 && deviceState === "all-on") return 100;
  const values = [0,25,50,75,100];
  return values[Math.round(Math.random() * values.length)];
};

const makeDevice = (index: number, deviceState: DeviceState) => {
  const lightBoolean = makeValueBoolean(deviceState);
  const lightInteger = makeValueInteger(deviceState, lightBoolean);
  const fanBoolean = makeValueBoolean(deviceState);
  const fanInteger = makeValueInteger(deviceState, fanBoolean);
  return {
    key: makeKey(index),
    productName: `Ayla ${Math.random()
      .toString(36)
      .substring(9)
      .toUpperCase()}`,
    dsn: `AC000Q3${makeKey(index)}`,
    properties: [
      {
        deviceKey: makeKey(index),
        baseType: "boolean",
        key: `${makeKey(index)}0`,
        name: "SET_LIGHT0_ON_CTL",
        value: lightBoolean
      },
      {
        deviceKey: makeKey(index),
        baseType: "integer",
        key: `${makeKey(index)}1`,
        name: "SET_LIGHT0_ILLUMINATION_CTL",
        value: lightInteger
      },
      {
        deviceKey: makeKey(index),
        baseType: "boolean",
        key: `${makeKey(index)}2`,
        name: "SET_FAN_ON_CTL",
        value: fanBoolean
      },
      {
        deviceKey: makeKey(index),
        baseType: "integer",
        key: `${makeKey(index)}3`,
        name: "SET_FAN_SPEED_CTL",
        value: fanInteger
      }
    ]
  };
};

export const makeDevices = (count: number, deviceState: DeviceState) => {
  let devices = [];
  for (let i = 0; i < count; i++) {
    devices.push(makeDevice(i, deviceState));
  }
  return devices;
};

export const devices = [device1, device2];
