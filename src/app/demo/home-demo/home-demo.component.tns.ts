import { Component } from "@angular/core";
import { makeDevices, DeviceState } from "./demo-data";

@Component({
  selector: "app-home-demo",
  template: `
    <app-home [devices]="devices" [hasLoaded]="true"></app-home>
  `
})
export class HomeDemoComponent {
  devices;
  count = 3;
  deviceState: DeviceState = "random";

  constructor() {
    this.devices = makeDevices(this.count, this.deviceState);
  }
}
