import { Component } from "@angular/core";
import { makeDevices, DeviceState } from "./demo-data";

@Component({
  selector: "app-home-demo",
  template: `
    <div>
      <label for="device-count">Number of Devices: </label>
      <input
        type="range"
        id="device-count"
        name="device-count"
        min="0"
        max="10"
        step="1"
        [defaultValue]="count"
        (change)="sliderChange($event)"
      />
      <input
        type="radio"
        id="all-off"
        name="device-states"
        value="all-off"
        (change)="radioChange($event)"
      />
      <label for="all-off">All off</label>
      <input
        type="radio"
        id="all-on"
        name="device-states"
        (change)="radioChange($event)"
        value="all-on"
      />
      <label for="all-on">All on</label>
      <input
        type="radio"
        id="random"
        name="device-states"
        (change)="radioChange($event)"
        value="random"
        checked
      />
      <label for="random">Random</label>
    </div>
    <app-home [devices]="devices" [hasLoaded]="true"></app-home>
  `
})
export class HomeDemoComponent {
  devices;
  count = 3;
  deviceState: DeviceState = "random";

  constructor() {
    this.devices = makeDevices(this.count, this.deviceState);
  }

  sliderChange(event) {
    this.count = event.target.value;
    this.devices = makeDevices(this.count, this.deviceState);
  }

  radioChange(event) {
    this.deviceState = event.target.value as DeviceState;
    this.devices = makeDevices(this.count, this.deviceState);
  }
}
