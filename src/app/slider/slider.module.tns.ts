import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SliderComponent } from "./slider.component";

@NgModule({
  declarations: [SliderComponent],
  imports: [NativeScriptCommonModule],
  exports: [SliderComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SliderModule {}
