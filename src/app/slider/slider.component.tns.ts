import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  AfterViewInit
} from "@angular/core";
import { Slider } from "tns-core-modules/ui/slider/slider";
import { PropertyChangeData } from "tns-core-modules/ui/page/page";

/**
 * Nativescript slider sucks
 * It doesn't support steps and it doesn't support on user changes
 */
@Component({
  selector: "app-slider",
  templateUrl: "./slider.component.html",
  styleUrls: ["./slider.component.scss"]
})
export class SliderComponent implements AfterViewInit {
  _value: number;
  @Output() valueChanged = new EventEmitter<number>();
  @ViewChild("slider", { static: true }) slider: ElementRef<Slider>;

  @Input() set value(value: number) {
    // Simulate stepping
    this._value = Math.round(value / 25);
  }

  get value(): number {
    return this._value;
  }

  ngAfterViewInit() {
    // Work around nativescript valueChange triggering itself
    this.slider.nativeElement.addEventListener(
      "valueChange",
      (args: PropertyChangeData) => {
        if (args.oldValue !== args.value) {
          this.valueChanged.emit(args.value * 25);
        }
      }
    );
  }
}
