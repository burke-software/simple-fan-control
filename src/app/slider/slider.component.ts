import { Component, Input, Output, EventEmitter } from "@angular/core";
import { MatSliderChange } from "@angular/material/slider";

@Component({
  selector: "app-slider",
  templateUrl: "./slider.component.html"
})
export class SliderComponent {
  @Input() value: number;
  @Output() valueChanged = new EventEmitter<number>();

  onSliderChange(change: MatSliderChange) {
    this.valueChanged.emit(change.value);
  }
}
