import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { registerElement } from "nativescript-angular/element-registry";
import { Carousel, CarouselItem } from "nativescript-carousel";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AlyaModule } from "./alya/alya.module";

require("nativescript-localstorage");
registerElement(
  "SvgImage",
  () => require("@teammaestro/nativescript-svg").SVGImage
);
registerElement("Carousel", () => Carousel);
registerElement("CarouselItem", () => CarouselItem);

@NgModule({
  declarations: [AppComponent],
  imports: [NativeScriptModule, AppRoutingModule, AlyaModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
