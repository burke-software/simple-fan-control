import { Routes } from "@angular/router";
import { PrivacyComponent } from "./privacy.component";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: PrivacyComponent
  }
];
