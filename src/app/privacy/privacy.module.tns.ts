import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { PrivacyRoutingModule } from "./privacy-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { PrivacyComponent } from "./privacy.component";

@NgModule({
  declarations: [PrivacyComponent],
  imports: [PrivacyRoutingModule, NativeScriptCommonModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PrivacyModule {}
