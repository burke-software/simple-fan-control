import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatCardModule } from "@angular/material/card";
import { MatToolbarModule } from "@angular/material/toolbar";

import { PrivacyRoutingModule } from "./privacy-routing.module";
import { PrivacyComponent } from "./privacy.component";

@NgModule({
	declarations: [PrivacyComponent],
	imports: [
		CommonModule,
		MatCardModule,
		MatToolbarModule,
		PrivacyRoutingModule
	]
})
export class PrivacyModule {}
