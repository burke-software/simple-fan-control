import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { DebugRoutingModule } from './debug-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { DebugComponent } from './debug.component';


@NgModule({
  declarations: [DebugComponent],
  imports: [
    DebugRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DebugModule { }
