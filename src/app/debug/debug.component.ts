import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DevicesService } from "../alya/devices.service";
import { baseHeaders } from "../alya/api.constants";
import { getDomain } from "../alya/domain-helper";
import { UsersService } from "../alya/users.service";

const basePath = "/apiv1";
const domain = "https://ads-field.aylanetworks.com";
const baseUrl = getDomain(domain, basePath);

@Component({
  template: `
    <h1>Devices</h1>
    {{ devices | json }}
    <h1>Properties</h1>
    {{ properties | json }}
    <h1>Metadata</h1>
    {{ metaDatas | json }}
    <h1>Groups</h1>
    {{ groups | json }}
    <h1>User</h1>
    {{ user | json }}
    <h1>Errors</h1>
    {{ errors | json }}
  `
})
export class DebugComponent implements OnInit {
  devices;
  properties = [];
  groups;
  metaDatas = [];
  user;
  errors = [];

  constructor(
    private devicesService: DevicesService,
    private usersService: UsersService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    const headers = {
      ...baseHeaders,
      Authorization: this.devicesService.accessToken
    };
    const url = baseUrl + "/devices";
    this.getGroups();
    this.getUserData();
    this.http
      .get(url, { headers })
      .toPromise()
      .then((devices: any) => {
        this.devices = devices;
        devices.map(device => {
          this.getProperties(device.device.dsn);
          this.getMetaData(device.device.dsn);
        });
      })
      .catch(err => this.errors.push(err));
  }

  getProperties(dsn: string) {
    const headers = {
      ...baseHeaders,
      Authorization: this.devicesService.accessToken
    };
    const url = baseUrl + "/dsns/" + dsn + "/properties";
    return this.http
      .get(url, { headers })
      .toPromise()
      .then(props => this.properties.push(props))
      .catch(err => this.errors.push(err));
  }

  getGroups() {
    const headers = {
      ...baseHeaders,
      Authorization: this.devicesService.accessToken
    };
    const url = baseUrl + "/groups/";
    this.http
      .get(url, { headers })
      .toPromise()
      .then(resp => (this.groups = resp))
      .catch(err => this.errors.push(err));
  }

  getMetaData(dsn: string) {
    const headers = {
      ...baseHeaders,
      Authorization: this.devicesService.accessToken
    };
    const url = baseUrl + "/dsns/" + dsn + "/data";
    this.http
      .get(url, { headers })
      .toPromise()
      .then(resp => this.metaDatas.push(resp))
      .catch(err => this.errors.push(err));
  }

  getUserData() {
    const userDomain = "https://user.aylanetworks.com";
    const userBasePath = "/users";
    const userBaseUrl = getDomain(userDomain, userBasePath);
    const headers = {
      Accept: "application/json",
      Authorization: "auth_token " + this.usersService.accessToken
    };
    const url = userBaseUrl + "/get_user_profile";
    this.http
      .get(url, { headers })
      .toPromise()
      .then(resp => (this.user = resp))
      .catch(err => this.errors.push(err));
  }
}
