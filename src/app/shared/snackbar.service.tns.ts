import { Injectable } from "@angular/core";
import { SnackBar } from "@nstudio/nativescript-snackbar";

@Injectable({
  providedIn: "root"
})
export class SnackbarService {
  snackbar = new SnackBar();

  show(message: string) {
    this.snackbar.simple(message);
  }
}
