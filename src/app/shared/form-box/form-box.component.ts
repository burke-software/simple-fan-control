import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  selector: "app-form-box",
  templateUrl: "./form-box.component.html",
  styleUrls: ["./form-box.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormBoxComponent {
  @Input() title: string;
  @Input() subtitle: string;
}
