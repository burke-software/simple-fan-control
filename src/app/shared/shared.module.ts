import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { MatCardModule } from "@angular/material/card";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatToolbarModule } from "@angular/material/toolbar";
import { FormBoxComponent } from "./form-box/form-box.component";
import { HeaderComponent } from "./header/header.component";
import { SnackbarService } from "./snackbar.service";
import { ErrorMessageComponent } from "./error-message/error-message.component";

@NgModule({
  declarations: [FormBoxComponent, HeaderComponent, ErrorMessageComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    MatSnackBarModule,
    MatToolbarModule
  ],
  exports: [FormBoxComponent, ErrorMessageComponent],
  providers: [SnackbarService]
})
export class SharedModule {}
