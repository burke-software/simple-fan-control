import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  @Input() text: string;
  @Input() type: "title" | "subtitle" = "title";
  /**
   * not a sensible default, except that it preserves the status quo prior
   * to adding it
   */
  @Input() bigBottomPad = true;
  @Input() alignCenter = true;
}
