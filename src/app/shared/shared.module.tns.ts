import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { FormBoxComponent } from "./form-box/form-box.component";
import { HeaderComponent } from "./header/header.component";
import { SnackbarService } from "./snackbar.service";
import { ErrorMessageComponent } from "./error-message/error-message.component";

@NgModule({
  declarations: [FormBoxComponent, HeaderComponent, ErrorMessageComponent],
  imports: [NativeScriptCommonModule],
  exports: [FormBoxComponent, HeaderComponent, ErrorMessageComponent],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [SnackbarService]
})
export class SharedModule {}
