import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { IsLoggedInGuard } from "../../../src/app/guards/is-logged-in.guard";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    // redirectTo: "/demo",
    pathMatch: "full"
  },
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then(mod => mod.HomeModule),
    canActivate: [IsLoggedInGuard]
  },
  {
    path: "demo",
    loadChildren: () => import("./demo/demo.module").then(mod => mod.DemoModule)
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then(mod => mod.LoginModule)
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
