import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { HomeRoutingModule } from "./home-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { HomeComponent } from "./home.component";
import { HomeContainer } from "./home.container";
import { SharedModule } from "../../../shared/shared.module";
import { SliderModule } from "../../../slider/slider.module";

@NgModule({
  declarations: [HomeComponent, HomeContainer],
  imports: [
    HomeRoutingModule,
    NativeScriptCommonModule,
    SharedModule,
    SliderModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [HomeComponent]
})
export class HomeModule {}
