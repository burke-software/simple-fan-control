import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { timer, EMPTY } from "rxjs";
import { concatMap, catchError } from "rxjs/operators";

import { HomeContainer as HomeContainerBase } from "../../../../src/app/home/home.container";
import { DevicesService } from "../../../../src/app/alya/devices.service";
import { UsersService } from "../../../../src/app/alya/users.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  template: `
    <ns-home
      [devices]="devices"
      [hasLoaded]="hasLoaded"
      (sliderChange)="onSliderChange($event)"
      (toggleProperty)="toggleProperty($event)"
      (signOut)="signOut()"
    ></ns-home>
  `
})
export class HomeContainer extends HomeContainerBase implements OnInit {
  constructor(
    public page: Page,
    public devicesService: DevicesService,
    public userService: UsersService
  ) {
    super(devicesService, userService);
    this.devicesService.getDevicesWithProperties.subscribe(
      devices => (this.devices = devices)
    );
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.devicesService
      .retrieveAll()
      .toPromise()
      .then(() => (this.hasLoaded = true));
    const pollInterval = 10000;
    this.polling$ = timer(pollInterval, pollInterval)
      .pipe(
        concatMap(() =>
          this.devicesService
            .updateProperties()
            .pipe(catchError((err: HttpErrorResponse) => EMPTY))
        )
      )
      .subscribe();
  }
}
