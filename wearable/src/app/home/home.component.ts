import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import {
  IDeviceWithProperties,
  IProperty
} from "../../../../src/app/alya/api.interfaces";

@Component({
  selector: "ns-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  @Input() devices: IDeviceWithProperties[] = [];
  @Input() hasLoaded = false;
  @Output() toggleProperty = new EventEmitter<IProperty>();
  @Output() sliderChange = new EventEmitter<{
    value: string;
    prop: IProperty;
  }>();
  @Output() setProperty = new EventEmitter<{
    property: IProperty;
    value: string;
  }>();
  @Output() signOut = new EventEmitter();

  constructor(private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  onSliderChange(value: number, prop: IProperty) {
    this.sliderChange.emit({ prop, value: value.toString() });
  }
}
