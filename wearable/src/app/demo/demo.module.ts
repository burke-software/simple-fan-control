import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { DemoRoutingModule } from "./demo-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { DemoComponent } from "./demo.component";
import { HomeModule } from "../home/home.module";

@NgModule({
  declarations: [DemoComponent],
  imports: [DemoRoutingModule, NativeScriptCommonModule, HomeModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DemoModule {}
