import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import {
  makeDevices,
  DeviceState
} from "../../../../src/app/demo/home-demo/demo-data";

@Component({
  template: `
    <ns-home [devices]="devices" [hasLoaded]="true"></ns-home>
  `
})
export class DemoComponent implements OnInit {
  deviceState: DeviceState = "random";
  devices = makeDevices(2, this.deviceState);

  constructor(private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }
}
