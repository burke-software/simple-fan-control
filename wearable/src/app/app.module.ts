import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { registerElement } from "nativescript-angular/element-registry";
import { WearOsLayout } from "nativescript-wear-os";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AlyaModule } from "../../alya/alya.module";
import { AuthService } from "../../alya/auth.service";
import { UsersService } from "../../alya/users.service";
import { DevicesService } from "../../alya/devices.service";
import { IsLoggedInGuard } from "../../guards/is-logged-in.guard";

require("nativescript-localstorage");
registerElement(
  "SvgImage",
  () => require("@teammaestro/nativescript-svg").SVGImage
);
registerElement("WearOsLayout", () => WearOsLayout);

@NgModule({
  bootstrap: [AppComponent],
  imports: [NativeScriptModule, AppRoutingModule, AlyaModule],
  declarations: [AppComponent],
  // Not clear why the Wear OS version needs these providers here while the mobile version doesn't
  providers: [AuthService, UsersService, DevicesService, IsLoggedInGuard],
  schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}
