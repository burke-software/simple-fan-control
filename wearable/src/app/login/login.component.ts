import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UsersService } from "../../../../src/app/alya/users.service";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "ns-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [
      Validators.required,
      Validators.minLength(6)
    ])
  });
  error = "";

  constructor(
    private page: Page,
    private api: UsersService,
    private router: Router
  ) {}

  ngOnInit() {
    this.error = "";
    this.page.actionBarHidden = true;
  }

  onSubmit() {
    this.error = "";
    if (this.form.valid) {
      this.api
        .signIn(this.form.value.email, this.form.value.password)
        .toPromise()
        .then(() => this.router.navigate(["/home"]))
        .catch((err: HttpErrorResponse) => {
          if (err.status === 401 && err.error && err.error.error) {
            this.error = err.error.error;
          } else {
            this.error =
              "Unable to log in.";
          }
        });
    } else {
      this.error = "Enter valid email and password";
    }
  }
}
